package com.bzs2.kiritrader;

public final class Utility {
    private Utility() {}

    public final static String SHARED_PREFERENCES_API_KEY = "apiKey";
    public final static String SHARED_PREFERENCES_SECRET_KEY = "secretKey";

    public static final String SHARED_PREFERENCES_FAVOURITE_COIN_SET
            = "shared_preferences_favourite_coin_set";

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
