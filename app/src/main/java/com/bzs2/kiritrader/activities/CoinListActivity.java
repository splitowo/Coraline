package com.bzs2.kiritrader.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.adapters.CoinsAdapter;
import com.bzs2.kiritrader.downloaders.MarketSummariesDownloader;
import com.bzs2.kiritrader.interfaces.MarketSummariesDownloaderListener;
import com.bzs2.kiritrader.jackson.JacksonMarketSummary;
import com.mancj.materialsearchbar.MaterialSearchBar;

public class CoinListActivity extends AppCompatActivity implements MarketSummariesDownloaderListener, CoinsAdapter.OnCoinClickListener,
        MaterialSearchBar.OnSearchActionListener, TextWatcher {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_list);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
        MaterialSearchBar searchBar = findViewById(R.id.searchBar);
        recyclerView = findViewById(R.id.recyclerView);
        searchBar.setOnSearchActionListener(this);
        searchBar.addTextChangeListener(this);
        ((CardView)findViewById(R.id.mt_container)).setRadius(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_button, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        new MarketSummariesDownloader(this).execute();
    }

    public void onMarketSummariesDownloaderFinished(JacksonMarketSummary marketSummaries[]) {
        if(marketSummaries != null) {
            recyclerView.swapAdapter(new CoinsAdapter(marketSummaries, this), false);
        }
        else {
            findViewById(R.id.no_network_message).setVisibility(View.VISIBLE);
        }
        findViewById(R.id.loading_frame).setVisibility(View.INVISIBLE);
    }

    public void onCoinClick(String coin) {
        Intent intent = new Intent(this, CoinDetailActivity.class);
        intent.putExtra(CoinDetailActivity.INTENT_EXTRA_COIN, coin);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_search:
                MaterialSearchBar searchBar = findViewById(R.id.searchBar);
                searchBar.setVisibility(View.VISIBLE);
                searchBar.enableSearch();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {
        if(!enabled) {
            MaterialSearchBar searchBar = findViewById(R.id.searchBar);
            searchBar.setVisibility(View.GONE);
            ((CoinsAdapter)recyclerView.getAdapter()).filter(null);
        }
    }

    @Override
    public void onSearchConfirmed(CharSequence text) {

    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        ((CoinsAdapter)recyclerView.getAdapter()).filter(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }
}
