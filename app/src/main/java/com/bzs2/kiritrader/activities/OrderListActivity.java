package com.bzs2.kiritrader.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.Utility;
import com.bzs2.kiritrader.adapters.OpenOrdersExtendedAdapter;
import com.bzs2.kiritrader.downloaders.CancelOrderDownloader;
import com.bzs2.kiritrader.downloaders.OpenOrdersDownloader;
import com.bzs2.kiritrader.interfaces.CancelOrderListener;
import com.bzs2.kiritrader.interfaces.OpenOrdersDownloaderListener;
import com.bzs2.kiritrader.jackson.JacksonOpenOrder;


public class OrderListActivity extends AppCompatActivity implements OpenOrdersDownloaderListener,
        OpenOrdersExtendedAdapter.OnOrderClickListener, CancelOrderListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshOpenOrders();
    }

    private void refreshOpenOrders() {
        if(apiKeysPresent()) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            new OpenOrdersDownloader(this).execute(
                    sharedPreferences.getString(Utility.SHARED_PREFERENCES_API_KEY, null),
                    sharedPreferences.getString(Utility.SHARED_PREFERENCES_SECRET_KEY, null));
        } else {
            TextView textView = findViewById(R.id.no_data_message_text);
            textView.setText(R.string.no_api_keys_order_list_activity);
            textView.setVisibility(View.VISIBLE);
            findViewById(R.id.loading_frame).setVisibility(View.INVISIBLE);
        }
    }

    public void onOrdersDownloaderFinished(JacksonOpenOrder[] openOrders) {
        if(openOrders != null && openOrders.length != 0) {
            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            recyclerView.swapAdapter(new OpenOrdersExtendedAdapter(openOrders, this), false);
        } else if (openOrders != null /* && openOrders.length == 0 */) {
            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            recyclerView.swapAdapter(new OpenOrdersExtendedAdapter(openOrders, this), false);
            ((TextView)findViewById(R.id.no_data_message_text)).setText(R.string.no_data_open_orders);
            findViewById(R.id.no_data_message_text).setVisibility(View.VISIBLE);
        } else /* openOrders == null */{
            findViewById(R.id.no_network_message).setVisibility(View.VISIBLE);
        }
        findViewById(R.id.loading_frame).setVisibility(View.INVISIBLE);
    }

    public void onInvalidApiKeys() {
        ((TextView)findViewById(R.id.no_network_message_text)).setText(R.string.invalid_api_keys);
        findViewById(R.id.no_network_message).setVisibility(View.VISIBLE);
    }

    public void onOrderCancelClick(String uuid) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        new CancelOrderDownloader(this).execute(
                sharedPreferences.getString(Utility.SHARED_PREFERENCES_API_KEY, null),
                sharedPreferences.getString(Utility.SHARED_PREFERENCES_SECRET_KEY, null),
                uuid);
    }

    public void onCancelOrderFinished(String success) {
        if(success.equals("true")) {
            Snackbar.make(findViewById(R.id.root), R.string.snackbar_order_cancelled_success, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(findViewById(R.id.root), R.string.snackbar_order_cancelled_failure, Snackbar.LENGTH_LONG).show();
        }

        refreshOpenOrders();
    }

    @Override
    public void onMarketNameClick(String marketName) {
        Intent intent = new Intent(this, CoinDetailActivity.class);
        intent.putExtra("coin", marketName);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean apiKeysPresent() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return(!(sharedPreferences.getString(Utility.SHARED_PREFERENCES_API_KEY, "").equals(""))
                && !(sharedPreferences.getString(Utility.SHARED_PREFERENCES_SECRET_KEY, "").equals("")));
    }
}
