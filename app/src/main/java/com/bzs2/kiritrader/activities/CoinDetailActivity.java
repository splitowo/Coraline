package com.bzs2.kiritrader.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.Utility;
import com.bzs2.kiritrader.adapters.OpenOrdersAdapter;
import com.bzs2.kiritrader.downloaders.BalanceDownloader;
import com.bzs2.kiritrader.downloaders.BuyLimitDownloader;
import com.bzs2.kiritrader.downloaders.CancelOrderDownloader;
import com.bzs2.kiritrader.downloaders.MarketSummaryDownloader;
import com.bzs2.kiritrader.downloaders.OpenOrdersDownloader;
import com.bzs2.kiritrader.downloaders.SellLimitDownloader;
import com.bzs2.kiritrader.fragments.InvalidOrderInputDialogFragment;
import com.bzs2.kiritrader.fragments.OrderCancellationConfirmationDialogFragment;
import com.bzs2.kiritrader.fragments.OrderConfirmationDialogFragment;
import com.bzs2.kiritrader.interfaces.BalanceDownloaderListener;
import com.bzs2.kiritrader.interfaces.BuySellLimitListener;
import com.bzs2.kiritrader.interfaces.CancelOrderListener;
import com.bzs2.kiritrader.interfaces.MarketSummaryDownloaderListener;
import com.bzs2.kiritrader.interfaces.OpenOrdersDownloaderListener;
import com.bzs2.kiritrader.jackson.JacksonBalance;
import com.bzs2.kiritrader.jackson.JacksonMarketSummary;
import com.bzs2.kiritrader.jackson.JacksonOpenOrder;
import com.bzs2.kiritrader.jackson.JacksonOrderUuid;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.bzs2.kiritrader.Utility.SHARED_PREFERENCES_API_KEY;
import static com.bzs2.kiritrader.Utility.SHARED_PREFERENCES_SECRET_KEY;

public class CoinDetailActivity extends AppCompatActivity implements MarketSummaryDownloaderListener,
        OpenOrdersDownloaderListener, BalanceDownloaderListener, BuySellLimitListener, CancelOrderListener,
        TextWatcher, OrderConfirmationDialogFragment.listener, OpenOrdersAdapter.OnOrderClickListener,
        OrderCancellationConfirmationDialogFragment.listener {

    /*
    future:
        refresh coinDetailActivity: place an indeterminate loading bar on the action bar for the duration of the operation
        refresh button in coinDetailActivity, to the upper right corner
    TODO
        order view at coin detail activity
     */

    public static final String INTENT_EXTRA_COIN = "coin";
    static final DecimalFormat decimalFormat = new DecimalFormat("0.00000000");
    // the exchange takes a fee of 0.25% of each transaction
    private static final Double BITTREX_FEE = 0.0025d;
    String coin;
    String masterCoin;
    String actualCoin;
    Double availableCurrency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin_detail);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        coin = getIntent().getStringExtra(INTENT_EXTRA_COIN);
        masterCoin = coin.substring(0, coin.indexOf('-'));
        actualCoin = coin.substring(coin.indexOf('-') + 1);
        onBuySellSwitch(false);
        Switch buySellSwitch = findViewById(R.id.buySellSwitch);
        buySellSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> onBuySellSwitch(isChecked));
        ((EditText)findViewById(R.id.editTextQuantity)).addTextChangedListener(this);
        ((EditText)findViewById(R.id.editTextRate)).addTextChangedListener(this);

        new MarketSummaryDownloader(this).execute(coin);
        if(apiKeysPresent())
            refreshOpenOrders();
        else {
            findViewById(R.id.cardView_order_new).setVisibility(View.GONE);
            findViewById(R.id.cardView_open_order).setVisibility(View.GONE);
            findViewById(R.id.api_keys_message).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onMarketSummaryDownloaderFinished(JacksonMarketSummary marketSummary) {
        if(marketSummary != null) {
            ((TextView) findViewById(R.id.coin_name)).setText(String.valueOf(marketSummary.getMarketName()));
            ((TextView) findViewById(R.id.last)).setText(String.valueOf(decimalFormat.format(marketSummary.getLast())));
            ((TextView) findViewById(R.id.volume)).setText(String.valueOf(decimalFormat.format(marketSummary.getVolume())));
            ((TextView) findViewById(R.id.bid)).setText(String.valueOf(decimalFormat.format(marketSummary.getBid())));
            ((TextView) findViewById(R.id.ask)).setText(String.valueOf(decimalFormat.format(marketSummary.getAsk())));
            ((TextView) findViewById(R.id.high24)).setText(String.valueOf(decimalFormat.format(marketSummary.getHigh())));
            ((TextView) findViewById(R.id.low24)).setText(String.valueOf(decimalFormat.format(marketSummary.getLow())));
        } else findViewById(R.id.no_network_message).setVisibility(View.VISIBLE);

        findViewById(R.id.loading_frame).setVisibility(View.INVISIBLE);
    }

    public void onBuySellSwitch(boolean isChecked) {
        CardView cardView = findViewById(R.id.cardView_order_new);
        cardView.setCardBackgroundColor(
                getResources().getColor(isChecked ?
                        R.color.sellCardBackground : R.color.buyCardBackground, null));

        ((TextView)findViewById(R.id.currency_available)).setText(null);

        if(apiKeysPresent()) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            new BalanceDownloader(this).execute(
                    sharedPreferences.getString(SHARED_PREFERENCES_API_KEY, null),
                    sharedPreferences.getString(SHARED_PREFERENCES_SECRET_KEY, null),
                    isChecked ? actualCoin : masterCoin);
        }

    }

    public void onBuySellButton(View view) {
        String quantity = ((EditText)findViewById(R.id.editTextQuantity)).getText().toString();
        String rate = ((EditText)findViewById(R.id.editTextRate)).getText().toString();

        try {
            Double.parseDouble(quantity);
            Double.parseDouble(rate);

            new OrderConfirmationDialogFragment()
                    .setOrderType(((Switch)findViewById(R.id.buySellSwitch)).isChecked())
                    .setListener(this)
                    .show(getSupportFragmentManager(), "order_confirmation");
        } catch (NumberFormatException exception) {
            new InvalidOrderInputDialogFragment().show(getSupportFragmentManager(), "order_invalid_input");
        }
    }

    public void onMaxQuantityButton(View view) {
        ((EditText)findViewById(R.id.editTextQuantity)).setText(String.valueOf(decimalFormat.format(availableCurrency)));

        if(((Switch)findViewById(R.id.buySellSwitch)).isChecked()) {
            // SELL
            ((EditText)findViewById(R.id.editTextQuantity)).setText(String.valueOf(decimalFormat.format(availableCurrency)));
        } else {
            // BUY
            EditText editTextRate = findViewById(R.id.editTextRate);
            String rate = editTextRate.getText().toString();
            if(rate.equals("")) editTextRate.setText(((TextView)findViewById(R.id.last)).getText());
            Double dRate = Double.parseDouble(editTextRate.getText().toString());

            ((EditText)findViewById(R.id.editTextQuantity)).setText(decimalFormat.format(
                    availableCurrency / (dRate * (1.0d + BITTREX_FEE))
                    ));
        }
    }

    @Override
    public void onBalanceDownloaderFinished(JacksonBalance jacksonBalance) {
        if(jacksonBalance != null) {
            availableCurrency = jacksonBalance.getAvailable();
            if (jacksonBalance.getBalance() == null)
                ((TextView) findViewById(R.id.currency_available)).setText(R.string.none);
            else
                ((TextView) findViewById(R.id.currency_available)).setText(String.valueOf(decimalFormat.format(availableCurrency)));
        }
    }

    @Override
    public void onBuySellLimitFinished(JacksonOrderUuid jacksonOrderUuid) {
        if(jacksonOrderUuid != null) {
            Snackbar.make(findViewById(R.id.root), R.string.snackbar_order_placed_success, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(findViewById(R.id.root), R.string.snackbar_order_placed_failure, Snackbar.LENGTH_LONG).show();
        }

        // if we refresh orders right away, the new order might not be returned
        // lets make a delay, then
        final Handler handler = new Handler();
        handler.postDelayed(this::refreshOpenOrders, 500);

        findViewById(R.id.place_order_loading).setVisibility(View.INVISIBLE);
        findViewById(R.id.place_order).setVisibility(View.VISIBLE);
    }

    @Override
    public void onCancelOrderFinished(String success) {
        if(success.equals("true")) {
            Snackbar.make(findViewById(R.id.root), R.string.snackbar_order_cancelled_success, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(findViewById(R.id.root), R.string.snackbar_order_cancelled_failure, Snackbar.LENGTH_LONG).show();
        }

        final Handler handler = new Handler();
        handler.postDelayed(this::refreshOpenOrders, 500);
    }

    public void afterTextChanged(Editable s) {}

    public void beforeTextChanged(CharSequence s, int start,
                                  int count, int after) {}

    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        TextView orderTotal = findViewById(R.id.order_total);
        try {
            orderTotal.setText(decimalFormat.format(
                    Double.parseDouble(((EditText) findViewById(R.id.editTextQuantity)).getText().toString())
                            * Double.parseDouble(((EditText) findViewById(R.id.editTextRate)).getText().toString())));
        } catch (NumberFormatException exception) {
            orderTotal.setText(null);
        }
    }

    public void onOrderConfirmationDialogPositiveResponse(boolean orderType) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String quantity = ((EditText)findViewById(R.id.editTextQuantity)).getText().toString();
        String rate = ((EditText)findViewById(R.id.editTextRate)).getText().toString();

        if(!orderType) {
            new BuyLimitDownloader(this).execute(
                    sharedPreferences.getString(SHARED_PREFERENCES_API_KEY, null),
                    sharedPreferences.getString(SHARED_PREFERENCES_SECRET_KEY, null),
                    getIntent().getStringExtra(INTENT_EXTRA_COIN), quantity, rate);
        }
        else {
            new SellLimitDownloader(this).execute(
                    sharedPreferences.getString(SHARED_PREFERENCES_API_KEY, null),
                    sharedPreferences.getString(SHARED_PREFERENCES_SECRET_KEY, null),
                    getIntent().getStringExtra(INTENT_EXTRA_COIN), quantity, rate);
        }

        findViewById(R.id.place_order).setVisibility(View.INVISIBLE);
        findViewById(R.id.place_order_loading).setVisibility(View.VISIBLE);
    }

    public void onOrderCancellationConfirmationDialogPositiveResponse(String uuid) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        new CancelOrderDownloader(this).execute(
                sharedPreferences.getString(SHARED_PREFERENCES_API_KEY, null),
                sharedPreferences.getString(SHARED_PREFERENCES_SECRET_KEY, null),
                uuid);
    }

    @Override
    public void onOrderCancelClick(String uuid) {
        new OrderCancellationConfirmationDialogFragment()
                .setOrderUuid(uuid)
                .setListener(this)
                .show(getSupportFragmentManager(), "order_cancellation_confirmation");
    }

    private void refreshOpenOrders() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        new OpenOrdersDownloader(this).execute(
                sharedPreferences.getString(SHARED_PREFERENCES_API_KEY, null),
                sharedPreferences.getString(SHARED_PREFERENCES_SECRET_KEY, null), coin);
    }

    @Override
    public void onOrdersDownloaderFinished(JacksonOpenOrder[] jacksonOpenOrders) {
        if(jacksonOpenOrders != null && jacksonOpenOrders.length != 0) {
            findViewById(R.id.no_data_message_text).setVisibility(View.GONE);
            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            recyclerView.swapAdapter(new OpenOrdersAdapter(jacksonOpenOrders, this), false);
        } else if (jacksonOpenOrders != null /* && jacksonOpenOrders.length == 0 */) {
            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            recyclerView.swapAdapter(new OpenOrdersAdapter(jacksonOpenOrders, this), false);
            findViewById(R.id.no_data_message_text).setVisibility(View.VISIBLE);
        } else /* jacksonOpenOrders == null */ {
            findViewById(R.id.no_network_message).setVisibility(View.VISIBLE);
        }
    }

    public void onInvalidApiKeys() {
        findViewById(R.id.cardView_order_new).setVisibility(View.GONE);
        findViewById(R.id.cardView_open_order).setVisibility(View.GONE);
        findViewById(R.id.api_keys_message).setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_coin_detail_menu, menu);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Set<String> favouriteCoinSet = sharedPreferences.getStringSet(Utility.SHARED_PREFERENCES_FAVOURITE_COIN_SET, null);
        if(favouriteCoinSet != null && favouriteCoinSet.contains(coin))
            ((Toolbar)findViewById(R.id.toolbar)).getMenu().findItem(R.id.action_favorite).setIcon(R.drawable.ic_star);

        return true;
    }


    private boolean apiKeysPresent() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return(!(sharedPreferences.getString(Utility.SHARED_PREFERENCES_API_KEY, "").equals(""))
                && !(sharedPreferences.getString(Utility.SHARED_PREFERENCES_SECRET_KEY, "").equals("")));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_favorite:
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                Set<String> favouriteCoinSet = new HashSet<>(sharedPreferences.getStringSet(Utility.SHARED_PREFERENCES_FAVOURITE_COIN_SET, Collections.emptySet()));
                if(favouriteCoinSet.contains(coin)) {
                    favouriteCoinSet.remove(coin);
                    sharedPreferences.edit()
                            .remove(Utility.SHARED_PREFERENCES_FAVOURITE_COIN_SET)
                            .putStringSet(Utility.SHARED_PREFERENCES_FAVOURITE_COIN_SET, favouriteCoinSet)
                            .apply();
                    ((Toolbar)findViewById(R.id.toolbar)).getMenu().findItem(R.id.action_favorite).setIcon(R.drawable.ic_star_border);
                    Snackbar.make(findViewById(R.id.root), R.string.snackbar_market_removed_favorite, Snackbar.LENGTH_SHORT).show();
                }
                else {
                    favouriteCoinSet.add(coin);
                    sharedPreferences.edit()
                            .remove(Utility.SHARED_PREFERENCES_FAVOURITE_COIN_SET)
                            .putStringSet(Utility.SHARED_PREFERENCES_FAVOURITE_COIN_SET, favouriteCoinSet)
                            .apply();
                    ((Toolbar)findViewById(R.id.toolbar)).getMenu().findItem(R.id.action_favorite).setIcon(R.drawable.ic_star);
                    Snackbar.make(findViewById(R.id.root), R.string.snackbar_market_added_favorite, Snackbar.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
