package com.bzs2.kiritrader.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.Utility;
import com.bzs2.kiritrader.adapters.BalancesAdapter;
import com.bzs2.kiritrader.adapters.FavoriteMarketAdapter;
import com.bzs2.kiritrader.downloaders.BalancesDownloader;
import com.bzs2.kiritrader.downloaders.MarketSummariesDownloader;
import com.bzs2.kiritrader.interfaces.BalancesDownloaderListener;
import com.bzs2.kiritrader.interfaces.MarketSummariesDownloaderListener;
import com.bzs2.kiritrader.jackson.JacksonBalance;
import com.bzs2.kiritrader.jackson.JacksonMarketSummary;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements BalancesDownloaderListener,
        MarketSummariesDownloaderListener, BalancesAdapter.OnBalanceClickListener,
        FavoriteMarketAdapter.OnCoinClickListener {

    private DrawerLayout mDrawerLayout;
    private JacksonBalance[] jacksonBalances;
    private JacksonMarketSummary[] marketSummaries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                MainActivity.this::onNavigationItemSelected
        );

        mDrawerLayout = findViewById(R.id.drawer_layout);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(apiKeysPresent()) {
            mDrawerLayout.closeDrawers();
            setContentView(R.layout.activity_main);
            setSupportActionBar(findViewById(R.id.toolbar));
            ActionBar actionBar = getSupportActionBar();
            if(actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }

            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(
                MainActivity.this::onNavigationItemSelected
            );

            mDrawerLayout = findViewById(R.id.drawer_layout);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            new BalancesDownloader(this).execute(
                    sharedPreferences.getString(Utility.SHARED_PREFERENCES_API_KEY, null),
                    sharedPreferences.getString(Utility.SHARED_PREFERENCES_SECRET_KEY, null));
        }
        else {
            findViewById(R.id.cardView0).setVisibility(View.GONE);
            findViewById(R.id.cardView1).setVisibility(View.GONE);
            findViewById(R.id.no_api_keys_message_text).setVisibility(View.VISIBLE);
            findViewById(R.id.loading_frame).setVisibility(View.INVISIBLE);
        }
        new MarketSummariesDownloader(this).execute();
    }

    public void onBalancesDownloaderFinished(JacksonBalance[] jacksonBalances) {
        this.jacksonBalances = null;
        if(jacksonBalances != null && jacksonBalances.length != 0) {
            RecyclerView recyclerView = findViewById(R.id.coins_owned);
            recyclerView.swapAdapter(new BalancesAdapter(jacksonBalances, this), false);
            this.jacksonBalances = jacksonBalances;
        }
        else if (jacksonBalances != null /* && jacksonBalances.length == 0 */ ) {
            findViewById(R.id.cardView1).setVisibility(View.GONE);
            findViewById(R.id.no_balances_message_text).setVisibility(View.VISIBLE);
        } else /* jacksonBalances == null */ {
            findViewById(R.id.no_network_message).setVisibility(View.VISIBLE);
        }
        computeOverallEstimatedValue();
        findViewById(R.id.loading_frame).setVisibility(View.INVISIBLE);
    }

    public void onMarketSummariesDownloaderFinished(JacksonMarketSummary[] marketSummaries) {
        findViewById(R.id.loading_frame).setVisibility(View.INVISIBLE);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Set<String> set = sharedPreferences.getStringSet(Utility.SHARED_PREFERENCES_FAVOURITE_COIN_SET, null);
        if(set == null || set.size() == 0) {
            findViewById(R.id.no_favourites_message_text).setVisibility(View.VISIBLE);
        } else if(marketSummaries != null) {
            findViewById(R.id.no_favourites_message_text).setVisibility(View.GONE);
            RecyclerView recyclerView = findViewById(R.id.coins_favourite);
            recyclerView.swapAdapter(new FavoriteMarketAdapter(marketSummaries, this, set),
                    false);
        }
        this.marketSummaries = marketSummaries;
        computeOverallEstimatedValue();
    }

    public void onInvalidApiKeys() {
        ((TextView)findViewById(R.id.no_api_keys_message_text)).setText(R.string.invalid_api_keys);
        findViewById(R.id.cardView0).setVisibility(View.GONE);
        findViewById(R.id.cardView1).setVisibility(View.GONE);
        findViewById(R.id.no_api_keys_message_text).setVisibility(View.VISIBLE);
        findViewById(R.id.loading_frame).setVisibility(View.INVISIBLE);
    }

    public void onBalanceClick(String coin) {}

    @Override
    public void onFavoriteMarketClick(String coin) {
        Intent intent = new Intent(this, CoinDetailActivity.class);
        intent.putExtra(CoinDetailActivity.INTENT_EXTRA_COIN, coin);
        startActivity(intent);
    }

    public void onBrowseAllButton(View view) {
        startActivity(new Intent(this, CoinListActivity.class));
    }

    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_drawer_home_screen:
                mDrawerLayout.closeDrawers();
                return true;
            case R.id.navigation_drawer_coin_list:
                startActivity(new Intent(this, CoinListActivity.class));
                mDrawerLayout.closeDrawers();
                return true;
            case R.id.navigation_drawer_open_orders:
                startActivity(new Intent(this, OrderListActivity.class));
                mDrawerLayout.closeDrawers();
                return true;
            case R.id.navigation_drawer_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                mDrawerLayout.closeDrawers();
                return true;
            case R.id.navigation_drawer_about:
                displayAboutDialog();
                mDrawerLayout.closeDrawers();
                return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void displayAboutDialog() {
        String version = "";
        try {
            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage(version.concat("\n\n").concat(getString(R.string.about_dialog_text)))
                .setPositiveButton(R.string.ok, null)
                .create();
        alertDialog.show();
    }

    private void computeOverallEstimatedValue() {
        if(jacksonBalances == null || marketSummaries == null) return;

        double btcBalance = 0;
        for (JacksonBalance jacksonBalance : jacksonBalances) {
            if(jacksonBalance.getCurrency().equals("BTC")) btcBalance += jacksonBalance.getBalance();
            else {
                Optional summary = Arrays.stream(marketSummaries).filter(jacksonMarketSummary ->
                        jacksonMarketSummary.getMarketName()
                                .equals("BTC-".concat(jacksonBalance.getCurrency())))
                        .findAny();
                if(summary.isPresent())
                    btcBalance += jacksonBalance.getBalance()
                            * ((JacksonMarketSummary)summary.get()).getLast();
            }
        }

        Optional summary = Arrays.stream(marketSummaries).filter(jacksonMarketSummary ->
                jacksonMarketSummary.getMarketName()
                        .equals("USDT-BTC")).findAny();

        if(summary.isPresent())
            ((TextView)findViewById(R.id.overall_estimated_value))
                    .setText("$".concat(new DecimalFormat("0.00")
                            .format(btcBalance
                                    * ((JacksonMarketSummary)summary.get()).getLast())));
    }

    private boolean apiKeysPresent() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return(!(sharedPreferences.getString(Utility.SHARED_PREFERENCES_API_KEY, "").equals(""))
                && !(sharedPreferences.getString(Utility.SHARED_PREFERENCES_SECRET_KEY, "").equals("")));
    }

}
