package com.bzs2.kiritrader.interfaces;

import com.bzs2.kiritrader.jackson.JacksonBalance;

public interface BalanceDownloaderListener {
    void onBalanceDownloaderFinished(JacksonBalance jacksonBalance);
}
