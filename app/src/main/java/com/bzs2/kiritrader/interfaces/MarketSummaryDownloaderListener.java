package com.bzs2.kiritrader.interfaces;

import com.bzs2.kiritrader.jackson.JacksonMarketSummary;

public interface MarketSummaryDownloaderListener {
    void onMarketSummaryDownloaderFinished(JacksonMarketSummary marketSummary);
}
