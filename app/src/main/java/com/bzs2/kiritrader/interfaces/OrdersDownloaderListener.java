package com.bzs2.kiritrader.interfaces;

import com.bzs2.kiritrader.jackson.JacksonOrder;

public interface OrdersDownloaderListener {
    void onOrdersDownloaderFinished(JacksonOrder[] jacksonOrders);
}
