package com.bzs2.kiritrader.interfaces;

public interface CancelOrderListener {
    void onCancelOrderFinished(String success);
}
