package com.bzs2.kiritrader.interfaces;

import com.bzs2.kiritrader.jackson.JacksonMarketSummary;

public interface MarketSummariesDownloaderListener {
    void onMarketSummariesDownloaderFinished(JacksonMarketSummary[] marketSummaries);
}
