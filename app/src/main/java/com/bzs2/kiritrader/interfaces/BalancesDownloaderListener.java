package com.bzs2.kiritrader.interfaces;

import com.bzs2.kiritrader.jackson.JacksonBalance;

public interface BalancesDownloaderListener {
    void onBalancesDownloaderFinished(JacksonBalance[] jacksonBalance);
    void onInvalidApiKeys();
}
