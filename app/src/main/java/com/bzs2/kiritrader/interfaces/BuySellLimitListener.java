package com.bzs2.kiritrader.interfaces;

import com.bzs2.kiritrader.jackson.JacksonOrderUuid;

public interface BuySellLimitListener {
    void onBuySellLimitFinished(JacksonOrderUuid jacksonTicker);
}
