package com.bzs2.kiritrader.interfaces;

import com.bzs2.kiritrader.jackson.JacksonTicker;

public interface TickersDownloaderListener {
    void onTickersDownloaderFinished(JacksonTicker[] jacksonTicker);
}
