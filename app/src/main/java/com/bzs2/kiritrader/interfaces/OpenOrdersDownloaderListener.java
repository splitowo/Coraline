package com.bzs2.kiritrader.interfaces;

import com.bzs2.kiritrader.jackson.JacksonOpenOrder;

public interface OpenOrdersDownloaderListener {
    void onOrdersDownloaderFinished(JacksonOpenOrder[] jacksonOpenOrders);
    void onInvalidApiKeys();
}
