package com.bzs2.kiritrader.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.jackson.JacksonMarketSummary;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.stream.Collectors;

public class CoinsAdapter extends RecyclerView.Adapter<CoinsAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private GridLayout gridLayout;
        OnClickListener listener;

        ViewHolder(GridLayout gridLayout, OnClickListener listener) {
            super(gridLayout);
            this.gridLayout = gridLayout;
            this.listener = listener;
            gridLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view instanceof GridLayout) {
                listener.onClick(((TextView)view.findViewById(R.id.coin_name)).getText().toString());
            }
        }

        interface OnClickListener {
            void onClick(String coin);
        }
    }

    private JacksonMarketSummary[] jacksonMarketSummaries;
    private OnCoinClickListener listener;
    private String filter = null;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GridLayout gridLayout = (GridLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coin_view, parent, false);
        return new ViewHolder(gridLayout, coin -> listener.onCoinClick(coin));
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        JacksonMarketSummary summary = filter == null ? jacksonMarketSummaries[position] :
                Arrays.stream(jacksonMarketSummaries).filter(
                jacksonMarketSummary -> jacksonMarketSummary.getMarketName().toLowerCase().contains(filter.toLowerCase())).collect(Collectors.toList()).get(position);

        GridLayout gridLayout = viewHolder.gridLayout;
        TextView textViewCoinName = gridLayout.findViewById(R.id.coin_name);
        TextView textViewCoinBalance = gridLayout.findViewById(R.id.coin_balance);
        TextView textViewCoinChange = gridLayout.findViewById(R.id.coin_change);
        textViewCoinName.setText(summary.getMarketName());
        textViewCoinBalance.setText(new DecimalFormat("0.00000000")
                .format(summary.getLast()));
    }

    public CoinsAdapter(JacksonMarketSummary[] jacksonMarketSummaries, OnCoinClickListener listener) {
        this.jacksonMarketSummaries = jacksonMarketSummaries;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return filter == null ? jacksonMarketSummaries.length :
                (int) Arrays.stream(jacksonMarketSummaries).filter(
                        jacksonMarketSummary -> jacksonMarketSummary.getMarketName().toLowerCase().contains(filter.toLowerCase())).count();
    }

    public interface OnCoinClickListener {
        void onCoinClick(String coin);
    }

    public void filter(String key) {
        filter = key;
        notifyDataSetChanged();
    }
}
