package com.bzs2.kiritrader.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.jackson.JacksonOrder;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder {
        private GridLayout gridLayout;

        ViewHolder(GridLayout gridLayout) {
            super(gridLayout);
            this.gridLayout = gridLayout;
        }
    }

    private JacksonOrder[] jacksonOrders;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GridLayout gridLayout = (GridLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_view, parent, false);
        return new ViewHolder(gridLayout);
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        GridLayout gridLayout = viewHolder.gridLayout;
        TextView textViewExchange = gridLayout.findViewById(R.id.exchange);
        TextView textViewPrice = gridLayout.findViewById(R.id.price);
        TextView textViewDate = gridLayout.findViewById(R.id.date);
        TextView textViewStatus = gridLayout.findViewById(R.id.status);
        textViewExchange.setText(jacksonOrders[position].getExchange());
        textViewPrice.setText(String.valueOf(jacksonOrders[position].getLimit()));
        textViewDate.setText(jacksonOrders[position].getTimeStamp());
        textViewStatus.setText("STATUS HERE");
    }

    public OrdersAdapter(JacksonOrder[] jacksonOrders) {
        this.jacksonOrders = jacksonOrders;
    }

    @Override
    public int getItemCount() {
        return jacksonOrders.length;
    }

}
