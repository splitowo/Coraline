package com.bzs2.kiritrader.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.jackson.JacksonMarketSummary;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Set;

public class FavoriteMarketAdapter extends RecyclerView.Adapter<FavoriteMarketAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private GridLayout gridLayout;
        OnClickListener listener;

        ViewHolder(GridLayout gridLayout, OnClickListener listener) {
            super(gridLayout);
            this.gridLayout = gridLayout;
            this.listener = listener;
            gridLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view instanceof GridLayout) {
                listener.onClick(((TextView)view.findViewById(R.id.coin_name)).getText().toString());
            }
        }

        interface OnClickListener {
            void onClick(String coin);
        }
    }

    private JacksonMarketSummary[] jacksonMarketSummaries;
    private OnCoinClickListener listener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GridLayout gridLayout = (GridLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coin_view, parent, false);
        return new ViewHolder(gridLayout, coin -> listener.onFavoriteMarketClick(coin));
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        GridLayout gridLayout = viewHolder.gridLayout;
        TextView textViewCoinName = gridLayout.findViewById(R.id.coin_name);
        TextView textViewCoinBalance = gridLayout.findViewById(R.id.coin_balance);
        TextView textViewCoinChange = gridLayout.findViewById(R.id.coin_change);
        textViewCoinName.setText(jacksonMarketSummaries[position].getMarketName());
        textViewCoinBalance.setText(new DecimalFormat("0.00000000")
                .format(jacksonMarketSummaries[position].getLast()));
    }

    public FavoriteMarketAdapter(JacksonMarketSummary[] jacksonMarketSummaries, OnCoinClickListener listener, Set<String> favorites) {
        if(favorites == null) this.jacksonMarketSummaries = null;
        else this.jacksonMarketSummaries = Arrays.stream(jacksonMarketSummaries)
                .filter(jacksonMarketSummary -> favorites.contains(jacksonMarketSummary.getMarketName()))
                .toArray(JacksonMarketSummary[]::new);
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return jacksonMarketSummaries.length;
    }

    public interface OnCoinClickListener {
        void onFavoriteMarketClick(String coin);
    }
}
