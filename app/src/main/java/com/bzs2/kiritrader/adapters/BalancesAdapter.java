package com.bzs2.kiritrader.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.jackson.JacksonBalance;


public class BalancesAdapter extends RecyclerView.Adapter<BalancesAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private GridLayout gridLayout;
        OnClickListener listener;

        ViewHolder(GridLayout gridLayout, OnClickListener listener) {
            super(gridLayout);
            this.gridLayout = gridLayout;
            this.listener = listener;
            //gridLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view instanceof GridLayout) {
                listener.onClick(((TextView)view.findViewById(R.id.coin_name)).getText().toString());
            }
        }

        interface OnClickListener {
            void onClick(String coin);
        }
    }

    private JacksonBalance[] jacksonBalances;
    private OnBalanceClickListener listener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GridLayout gridLayout = (GridLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coin_view, parent, false);
        return new ViewHolder(gridLayout, coin -> listener.onBalanceClick(coin));
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        GridLayout gridLayout = viewHolder.gridLayout;
        TextView textViewCoinName = gridLayout.findViewById(R.id.coin_name);
        TextView textViewCoinBalance = gridLayout.findViewById(R.id.coin_balance);
        TextView textViewCoinChange = gridLayout.findViewById(R.id.coin_change);
        textViewCoinName.setText(jacksonBalances[position].getCurrency());
        textViewCoinBalance.setText(String.valueOf(jacksonBalances[position].getBalance()));
    }

    public BalancesAdapter(JacksonBalance[] jacksonBalances, OnBalanceClickListener listener) {
        this.jacksonBalances = jacksonBalances;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return jacksonBalances.length;
    }

    public interface OnBalanceClickListener {
        void onBalanceClick(String coin);
    }
}
