package com.bzs2.kiritrader.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.jackson.JacksonOpenOrder;

import java.text.DecimalFormat;

public class OpenOrdersAdapter extends RecyclerView.Adapter<OpenOrdersAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private GridLayout gridLayout;
        OnClickListener listener;
        private String orderUuid;

        ViewHolder(GridLayout gridLayout, OnClickListener listener) {
            super(gridLayout);
            this.gridLayout = gridLayout;
            this.listener = listener;
            gridLayout.findViewById(R.id.button_cancel_order).setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(orderUuid);
        }

        interface OnClickListener {
            void onClick(String uuid);
        }
    }

    private static DecimalFormat decimalFormat = new DecimalFormat("0.00000000");
    private JacksonOpenOrder[] jacksonOpenOrders;
    private OnOrderClickListener listener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GridLayout gridLayout = (GridLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.open_order_view, parent, false);
        return new ViewHolder(gridLayout, uuid -> listener.onOrderCancelClick(uuid));
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        GridLayout gridLayout = viewHolder.gridLayout;
        TextView textViewBuySell =  gridLayout.findViewById(R.id.buySell);
        TextView textViewQuantity =  gridLayout.findViewById(R.id.quantity);
        TextView textViewPrice =  gridLayout.findViewById(R.id.price);

        if (jacksonOpenOrders[position].getOrderType().contains("SELL"))
            textViewBuySell.setText(R.string.sell);
        if (jacksonOpenOrders[position].getOrderType().contains("BUY"))
            textViewBuySell.setText(R.string.buy);
        textViewQuantity.setText(String.valueOf(jacksonOpenOrders[position].getQuantity()));
        textViewPrice.setText(decimalFormat.format((jacksonOpenOrders[position].getLimit())));
        viewHolder.orderUuid = jacksonOpenOrders[position].getOrderUuid();
    }

    public OpenOrdersAdapter(JacksonOpenOrder[] jacksonOrders, OnOrderClickListener listener) {
        this.jacksonOpenOrders = jacksonOrders;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return jacksonOpenOrders.length;
    }

    public interface OnOrderClickListener {
        void onOrderCancelClick(String uuid);
    }
}
