package com.bzs2.kiritrader.adapters;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.bzs2.kiritrader.R;
import com.bzs2.kiritrader.jackson.JacksonOpenOrder;

import java.text.DecimalFormat;

public class OpenOrdersExtendedAdapter extends RecyclerView.Adapter<OpenOrdersExtendedAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private GridLayout gridLayout;
        OnClickListener listener;
        private String orderUuid;
        private String marketName;

        ViewHolder(GridLayout gridLayout, OnClickListener listener) {
            super(gridLayout);
            this.gridLayout = gridLayout;
            this.listener = listener;
            gridLayout.findViewById(R.id.button_cancel_order).setOnClickListener(this);
            gridLayout.findViewById(R.id.marketName).setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.button_cancel_order) listener.onOrderCancelClick(orderUuid);
            if(view.getId() == R.id.marketName) listener.onMarketNameClick(marketName);
        }

        interface OnClickListener {
            void onOrderCancelClick(String uuid);
            void onMarketNameClick(String marketName);
        }
    }

    private static DecimalFormat decimalFormat = new DecimalFormat("0.00000000");
    private JacksonOpenOrder[] jacksonOpenOrders;
    private OnOrderClickListener listener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GridLayout gridLayout = (GridLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.open_order_extended_view, parent, false);
        return new ViewHolder(gridLayout, new ViewHolder.OnClickListener() {
            @Override
            public void onOrderCancelClick(String uuid) {
                listener.onOrderCancelClick(uuid);
            }

            @Override
            public void onMarketNameClick(String marketName) {
                listener.onMarketNameClick(marketName);
            }
        });
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        GridLayout gridLayout = viewHolder.gridLayout;
        TextView textViewMarketName =  gridLayout.findViewById(R.id.marketName);
        TextView textViewQuantity =  gridLayout.findViewById(R.id.quantity);
        TextView textViewPrice =  gridLayout.findViewById(R.id.price);
        String buySell = null;

        textViewMarketName.setText(jacksonOpenOrders[position].getExchange());
        if (jacksonOpenOrders[position].getOrderType().contains("SELL"))
            buySell = "SELL";
        if (jacksonOpenOrders[position].getOrderType().contains("BUY"))
            buySell = "BUY";
        textViewQuantity.setText(String.format("%s %s", buySell, String.valueOf(jacksonOpenOrders[position].getQuantity())));
        textViewPrice.setText(String.format(((Activity)listener).getResources().getString(R.string.open_order_price),
                decimalFormat.format((jacksonOpenOrders[position].getLimit()))));
        viewHolder.orderUuid = jacksonOpenOrders[position].getOrderUuid();
        viewHolder.marketName = jacksonOpenOrders[position].getExchange();
    }

    public OpenOrdersExtendedAdapter(JacksonOpenOrder[] jacksonOrders, OnOrderClickListener listener) {
        this.jacksonOpenOrders = jacksonOrders;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return jacksonOpenOrders.length;
    }

    public interface OnOrderClickListener {
        void onOrderCancelClick(String uuid);
        void onMarketNameClick(String marketName);
    }
}
