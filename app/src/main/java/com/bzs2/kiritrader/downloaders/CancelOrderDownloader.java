package com.bzs2.kiritrader.downloaders;

import android.os.AsyncTask;
import android.util.Log;

import com.bzs2.kiritrader.Utility;
import com.bzs2.kiritrader.interfaces.CancelOrderListener;
import com.bzs2.kiritrader.jackson.JacksonResponseCancelOrder;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class CancelOrderDownloader extends AsyncTask<String, Void, String> {
    private CancelOrderListener listener;
    protected String doInBackground(String arg[]) {
        try {
            Long nonce = System.currentTimeMillis();
            final String url = "https://bittrex.com/api/v1.1/market/cancel?apikey=" + arg[0] + "&uuid=" + arg[2]
                    + "&nonce=" + nonce;
            byte[] byteSecret = arg[1].getBytes();
            SecretKeySpec keySpec = new SecretKeySpec(byteSecret, "HmacSHA512");

            Mac sha512_HMAC = Mac.getInstance("HmacSHA512");
            sha512_HMAC.init(keySpec);
            byte[] macData = sha512_HMAC.doFinal(url.getBytes("UTF-8"));
            String result = Utility.bytesToHex(macData);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("apisign", result);
            HttpEntity<String> httpEntity = new HttpEntity<>("parameters", httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            return restTemplate.exchange(url, HttpMethod.POST,
                    httpEntity, JacksonResponseCancelOrder.class).getBody().getSuccess();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("BalancesDownloader", e.getMessage(), e);
        }

        return null;
    }

    public CancelOrderDownloader(CancelOrderListener listener) {
        super();
        this.listener = listener;
    }

    protected void onPostExecute(String success) {
        listener.onCancelOrderFinished(success);
    }
}
