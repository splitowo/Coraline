package com.bzs2.kiritrader.downloaders;

import android.os.AsyncTask;
import android.util.Log;

import com.bzs2.kiritrader.Utility;
import com.bzs2.kiritrader.interfaces.BalancesDownloaderListener;
import com.bzs2.kiritrader.jackson.JacksonBalance;
import com.bzs2.kiritrader.jackson.JacksonResponseBalances;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class BalancesDownloader extends AsyncTask<String, Void, JacksonBalance[]> {
    private BalancesDownloaderListener listener;
    private boolean invalidSignature;

    protected JacksonBalance[] doInBackground(String arg[]) {
        try {
            Long nonce = System.currentTimeMillis();
            final String url = "https://bittrex.com/api/v1.1/account/getbalances?apikey=" + arg[0]
                    + "&nonce=" + nonce;
            byte[] byteSecret = arg[1].getBytes();
            SecretKeySpec keySpec = new SecretKeySpec(byteSecret, "HmacSHA512");

            Mac sha512_HMAC = Mac.getInstance("HmacSHA512");
            sha512_HMAC.init(keySpec);
            byte[] macData = sha512_HMAC.doFinal(url.getBytes("UTF-8"));
            String result = Utility.bytesToHex(macData);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("apisign", result);
            HttpEntity<String> httpEntity = new HttpEntity<>("parameters", httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            JacksonResponseBalances jacksonResponseBalances = restTemplate.exchange(url, HttpMethod.POST,
                    httpEntity, JacksonResponseBalances.class).getBody();
            if(jacksonResponseBalances.getMessage().equals("INVALID_SIGNATURE")) {
                invalidSignature = true;
                return null;
            }
            JacksonBalance[] jacksonBalances = jacksonResponseBalances.getResult();
            return Arrays.stream(jacksonBalances).filter(w -> w.getBalance() != 0).toArray(JacksonBalance[]::new);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("BalancesDownloader", e.getMessage(), e);
        }

        return null;
    }

    public BalancesDownloader(BalancesDownloaderListener listener) {
        super();
        this.listener = listener;
        this.invalidSignature = false;
    }

    protected void onPostExecute(JacksonBalance[] jacksonBalances) {
        if(invalidSignature) listener.onInvalidApiKeys();
        else listener.onBalancesDownloaderFinished(jacksonBalances);
    }
}
