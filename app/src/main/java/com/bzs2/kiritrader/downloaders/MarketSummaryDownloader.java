package com.bzs2.kiritrader.downloaders;

import android.os.AsyncTask;
import android.util.Log;

import com.bzs2.kiritrader.interfaces.MarketSummaryDownloaderListener;
import com.bzs2.kiritrader.jackson.JacksonMarketSummary;
import com.bzs2.kiritrader.jackson.JacksonResponseMarketSummary;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class MarketSummaryDownloader extends AsyncTask<String, Void, JacksonMarketSummary[]> {
    private MarketSummaryDownloaderListener listener;
    protected JacksonMarketSummary[] doInBackground(String args[]) {
        try {
            final String url = "https://bittrex.com/api/v1.1/public/getmarketsummary?market=" + args[0];
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            return restTemplate.getForObject(url, JacksonResponseMarketSummary.class).getResult();
        } catch (Exception e) {
            Log.e("BalancesDownloader", e.getMessage(), e);
        }

        return null;
    }

    public MarketSummaryDownloader(MarketSummaryDownloaderListener listener) {
        super();
        this.listener = listener;
    }

    protected void onPostExecute(JacksonMarketSummary[] marketSummaries) {
        if(marketSummaries != null) listener.onMarketSummaryDownloaderFinished(marketSummaries[0]);
    }
}
