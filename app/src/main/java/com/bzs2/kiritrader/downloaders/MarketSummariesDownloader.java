package com.bzs2.kiritrader.downloaders;

import android.os.AsyncTask;
import android.util.Log;

import com.bzs2.kiritrader.interfaces.MarketSummariesDownloaderListener;
import com.bzs2.kiritrader.jackson.JacksonMarketSummary;
import com.bzs2.kiritrader.jackson.JacksonResponseMarketSummaries;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Set;

public class MarketSummariesDownloader extends AsyncTask<Void, Void, JacksonMarketSummary[]> {

    private MarketSummariesDownloaderListener listener;
    private Set<String> favourites;

    protected JacksonMarketSummary[] doInBackground(Void args[]) {
        try {
            final String url = "https://bittrex.com/api/v1.1/public/getmarketsummaries";
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            JacksonMarketSummary[] summaries =  restTemplate.getForObject(url, JacksonResponseMarketSummaries.class).getResult();

            if(favourites == null) return summaries;
            else
                return Arrays.stream(summaries)
                        .filter(jacksonMarketSummary -> favourites.contains(jacksonMarketSummary.getMarketName()))
                        .toArray(JacksonMarketSummary[]::new);

        } catch (Exception e) {
            Log.e("BalancesDownloader", e.getMessage(), e);
        }

        return null;
    }

    public MarketSummariesDownloader(MarketSummariesDownloaderListener listener) {
        super();
        this.listener = listener;
        this.favourites = null;
    }

    public MarketSummariesDownloader(MarketSummariesDownloaderListener listener, Set<String> favourites) {
        super();
        this.listener = listener;
        this.favourites = favourites;
    }

    protected void onPostExecute(JacksonMarketSummary[] marketSummaries) {
        listener.onMarketSummariesDownloaderFinished(marketSummaries);
    }
}
