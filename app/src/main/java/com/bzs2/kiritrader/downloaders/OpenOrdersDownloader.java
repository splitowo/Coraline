package com.bzs2.kiritrader.downloaders;

import android.os.AsyncTask;
import android.util.Log;

import com.bzs2.kiritrader.Utility;
import com.bzs2.kiritrader.interfaces.OpenOrdersDownloaderListener;
import com.bzs2.kiritrader.jackson.JacksonOpenOrder;
import com.bzs2.kiritrader.jackson.JacksonResponseOpenOrders;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class OpenOrdersDownloader extends AsyncTask<String, Void, JacksonOpenOrder[]> {
    private OpenOrdersDownloaderListener listener;
    private boolean invalidSignature;

    protected JacksonOpenOrder[] doInBackground(String arg[]) {
        try {
            Long nonce = System.currentTimeMillis();
            final String url = "https://bittrex.com/api/v1.1/market/getopenorders?apikey=" + arg[0]
                    + (arg.length >= 3 ? "&market=" + arg[2] : "") + "&nonce=" + nonce;
            byte[] byteSecret = arg[1].getBytes();
            SecretKeySpec keySpec = new SecretKeySpec(byteSecret, "HmacSHA512");

            Mac sha512_HMAC = Mac.getInstance("HmacSHA512");
            sha512_HMAC.init(keySpec);
            byte[] macData = sha512_HMAC.doFinal(url.getBytes("UTF-8"));
            String result = Utility.bytesToHex(macData);

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("apisign", result);
            HttpEntity<String> httpEntity = new HttpEntity<>("parameters", httpHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            JacksonResponseOpenOrders jacksonResponseOpenOrders = restTemplate.exchange(url, HttpMethod.POST,
                    httpEntity, JacksonResponseOpenOrders.class).getBody();
            if(jacksonResponseOpenOrders.getMessage().equals("INVALID_SIGNATURE")) {
                invalidSignature = true;
                return null;
            }

            return jacksonResponseOpenOrders.getResult();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("BalancesDownloader", e.getMessage(), e);
        }

        return null;
    }

    public OpenOrdersDownloader(OpenOrdersDownloaderListener listener) {
        super();
        this.listener = listener;
        this.invalidSignature = false;
    }

    protected void onPostExecute(JacksonOpenOrder[] jacksonOpenOrders) {
        if(invalidSignature) listener.onInvalidApiKeys();
        else listener.onOrdersDownloaderFinished(jacksonOpenOrders);
    }
}
