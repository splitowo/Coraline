package com.bzs2.kiritrader.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.bzs2.kiritrader.R;

public class OrderConfirmationDialogFragment extends DialogFragment {
    private boolean orderType;
    private listener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(orderType ? R.string.place_sell_order_confirmation : R.string.place_buy_order_confirmation)
                .setPositiveButton(R.string.send, (dialog, id) -> listener.onOrderConfirmationDialogPositiveResponse(orderType))
                .setNegativeButton(R.string.cancel, null);
        return builder.create();
    }

    public OrderConfirmationDialogFragment setOrderType(boolean sell) {
        orderType = sell;
        return this;
    }

    public OrderConfirmationDialogFragment setListener(listener listener) {
        this.listener = listener;
        return this;
    }

    public interface listener {
        void onOrderConfirmationDialogPositiveResponse(boolean orderType);
    }
}
