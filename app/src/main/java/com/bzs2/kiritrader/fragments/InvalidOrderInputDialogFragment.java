package com.bzs2.kiritrader.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.bzs2.kiritrader.R;

public class InvalidOrderInputDialogFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.place_order_invalid_numbers_dialog).setPositiveButton(R.string.ok, null);
        return builder.create();
    }
}
