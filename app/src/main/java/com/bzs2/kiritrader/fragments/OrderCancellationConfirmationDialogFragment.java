package com.bzs2.kiritrader.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.bzs2.kiritrader.R;

public class OrderCancellationConfirmationDialogFragment extends DialogFragment {
    private String uuid;
    private listener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.cancel_order_confirmation)
                .setPositiveButton(R.string.cancel_order_confirmation_cancel_it,
                        (dialog, id) -> listener.onOrderCancellationConfirmationDialogPositiveResponse(uuid))
                .setNegativeButton(R.string.cancel_order_confirmation_keep_it, null);
        return builder.create();
    }

    public OrderCancellationConfirmationDialogFragment setOrderUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public OrderCancellationConfirmationDialogFragment setListener(listener listener) {
        this.listener = listener;
        return this;
    }

    public interface listener {
        void onOrderCancellationConfirmationDialogPositiveResponse(String uuid);
    }
}
