package com.bzs2.kiritrader.jackson;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */
public class JacksonResponseMarketSummaries {
    private String success;
    private String message;
    private JacksonMarketSummary[] result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JacksonMarketSummary[] getResult() {
        return result;
    }

    public void setResult(JacksonMarketSummary[] result) {
        this.result = result;
    }
}
