package com.bzs2.kiritrader.jackson;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */
public class JacksonResponseBuySellLimit {
    private String success;
    private String message;
    private JacksonOrderUuid result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JacksonOrderUuid getResult() {
        return result;
    }

    public void setResult(JacksonOrderUuid result) {
        this.result = result;
    }

    public JacksonResponseBuySellLimit() {}
}
