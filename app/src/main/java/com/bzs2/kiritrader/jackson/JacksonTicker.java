package com.bzs2.kiritrader.jackson;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */

public class JacksonTicker {
    public Double getBid() {
        return bid;
    }

    public void setBid(Double bid) {
        this.bid = bid;
    }

    public Double getAsk() {
        return ask;
    }

    public void setAsk(Double ask) {
        this.ask = ask;
    }

    public Double getLast() {
        return last;
    }

    public void setLast(Double last) {
        this.last = last;
    }

    /* capital first letter is non-standard, therefore annotations are required */
    @JsonProperty("Bid")
    private Double bid;
    @JsonProperty("Ask")
    private Double ask;
    @JsonProperty("Last")
    private Double last;

    public JacksonTicker() {
    }
}