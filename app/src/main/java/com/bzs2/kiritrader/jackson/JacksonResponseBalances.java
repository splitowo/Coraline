package com.bzs2.kiritrader.jackson;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */
public class JacksonResponseBalances {
    private String success;
    private String message;
    private JacksonBalance[] result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JacksonBalance[] getResult() {
        return result;
    }

    public void setResult(JacksonBalance[] result) {
        this.result = result;
    }

    public JacksonResponseBalances() {}
}
