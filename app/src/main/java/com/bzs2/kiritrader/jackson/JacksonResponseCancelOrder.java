package com.bzs2.kiritrader.jackson;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */
public class JacksonResponseCancelOrder {
    private String success;
    private String message;
    private String result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public JacksonResponseCancelOrder() {}
}
