package com.bzs2.kiritrader.jackson;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */

public class JacksonBalance {
    @JsonProperty("Currency")
    private String currency;
    @JsonProperty("Balance")
    private Double balance;
    @JsonProperty("Available")
    private Double available;
    @JsonProperty("Pending")
    private Double pending;
    @JsonProperty("CryptoAddress")
    private String cryptoAddress;
    @JsonProperty("Requested")
    private boolean requested;
    @JsonProperty("Uuid")
    private String uuid;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getAvailable() {
        return available;
    }

    public void setAvailable(Double available) {
        this.available = available;
    }

    public Double getPending() {
        return pending;
    }

    public void setPending(Double pending) {
        this.pending = pending;
    }

    public String getCryptoAddress() {
        return cryptoAddress;
    }

    public void setCryptoAddress(String cryptoAddress) {
        this.cryptoAddress = cryptoAddress;
    }

    public boolean isRequested() {
        return requested;
    }

    public void setRequested(boolean requested) {
        this.requested = requested;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
