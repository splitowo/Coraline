package com.bzs2.kiritrader.jackson;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */
public class JacksonResponseOpenOrders {
    private String success;
    private String message;
    private JacksonOpenOrder[] result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JacksonOpenOrder[] getResult() {
        return result;
    }

    public void setResult(JacksonOpenOrder[] result) {
        this.result = result;
    }

    public JacksonResponseOpenOrders() {}
}
