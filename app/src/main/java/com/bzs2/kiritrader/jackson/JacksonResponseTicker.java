package com.bzs2.kiritrader.jackson;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */
class JacksonResponseTicker {
    private String success;
    private String message;
    private JacksonTicker result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JacksonTicker getResult() {
        return result;
    }

    public void setResult(JacksonTicker result) {
        this.result = result;
    }

    public JacksonResponseTicker() {}
}
