package com.bzs2.kiritrader.jackson;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */
public class JacksonResponseOrder {
    private String success;
    private String message;
    private JacksonOrder[] result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JacksonOrder[] getResult() {
        return result;
    }

    public void setResult(JacksonOrder[] result) {
        this.result = result;
    }

    public JacksonResponseOrder() {}
}
