package com.bzs2.kiritrader.jackson;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */

public class JacksonOrder {

    /* capital first letter is non-standard, therefore annotations are required */
    @JsonProperty("OrderUuid")
    private String orderUuid;
    @JsonProperty("Exchange")
    private String exchange;
    @JsonProperty("TimeStamp")
    private String timeStamp;
    @JsonProperty("OrderType")
    private String orderType;
    @JsonProperty("Limit")
    private double limit;
    @JsonProperty("Quantity")
    private double quantity;
    @JsonProperty("QuantityRemaining")
    private double quantityRemaining;
    @JsonProperty("Commission")
    private double commission;
    @JsonProperty("Price")
    private double price;
    @JsonProperty("PricePerUnit")
    private double pricePerUnit;
    @JsonProperty("IsConditional")
    private boolean isConditional;
    @JsonProperty("Condition")
    private String condition;
    @JsonProperty("ConditionTarget")
    private String conditionTarget;
    @JsonProperty("ImmediateOrCancel")
    private boolean immediateOrCancel;

    public JacksonOrder() {
    }

    public String getOrderUuid() {
        return orderUuid;
    }

    public void setOrderUuid(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getQuantityRemaining() {
        return quantityRemaining;
    }

    public void setQuantityRemaining(double quantityRemaining) {
        this.quantityRemaining = quantityRemaining;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public boolean isConditional() {
        return isConditional;
    }

    public void setConditional(boolean conditional) {
        isConditional = conditional;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getConditionTarget() {
        return conditionTarget;
    }

    public void setConditionTarget(String conditionTarget) {
        this.conditionTarget = conditionTarget;
    }

    public boolean isImmediateOrCancel() {
        return immediateOrCancel;
    }

    public void setImmediateOrCancel(boolean immediateOrCancel) {
        this.immediateOrCancel = immediateOrCancel;
    }
}