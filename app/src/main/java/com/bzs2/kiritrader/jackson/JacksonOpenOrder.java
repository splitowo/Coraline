package com.bzs2.kiritrader.jackson;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */

public class JacksonOpenOrder {

    /* capital first letter is non-standard, therefore annotations are required */
    @JsonProperty("Uuid")
    private String uuid;
    @JsonProperty("OrderUuid")
    private String orderUuid;
    @JsonProperty("Exchange")
    private String exchange;
    @JsonProperty("OrderType")
    private String orderType;
    @JsonProperty("Quantity")
    private double quantity;
    @JsonProperty("QuantityRemaining")
    private double quantityRemaining;
    @JsonProperty("Limit")
    private double limit;
    @JsonProperty("CommissionPaid")
    private double commissionPaid;
    @JsonProperty("Price")
    private double price;
    @JsonProperty("PricePerUnit")
    private double pricePerUnit;
    @JsonProperty("Opened")
    private String opened;
    @JsonProperty("Closed")
    private String closed;
    @JsonProperty("CancelInitiated")
    private boolean cancelInitiated;
    @JsonProperty("ImmediateOrCancel")
    private boolean immediateOrCancel;
    @JsonProperty("IsConditional")
    private boolean isConditional;
    @JsonProperty("Condition")
    private String condition;
    @JsonProperty("ConditionTarget")
    private String conditionTarget;

    public JacksonOpenOrder() {
    }


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getOrderUuid() {
        return orderUuid;
    }

    public void setOrderUuid(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getQuantityRemaining() {
        return quantityRemaining;
    }

    public void setQuantityRemaining(double quantityRemaining) {
        this.quantityRemaining = quantityRemaining;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }

    public double getCommissionPaid() {
        return commissionPaid;
    }

    public void setCommissionPaid(double commissionPaid) {
        this.commissionPaid = commissionPaid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public String getOpened() {
        return opened;
    }

    public void setOpened(String opened) {
        this.opened = opened;
    }

    public String getClosed() {
        return closed;
    }

    public void setClosed(String closed) {
        this.closed = closed;
    }

    public boolean isCancelInitiated() {
        return cancelInitiated;
    }

    public void setCancelInitiated(boolean cancelInitiated) {
        this.cancelInitiated = cancelInitiated;
    }

    public boolean isImmediateOrCancel() {
        return immediateOrCancel;
    }

    public void setImmediateOrCancel(boolean immediateOrCancel) {
        this.immediateOrCancel = immediateOrCancel;
    }

    public boolean isConditional() {
        return isConditional;
    }

    public void setConditional(boolean conditional) {
        isConditional = conditional;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getConditionTarget() {
        return conditionTarget;
    }

    public void setConditionTarget(String conditionTarget) {
        this.conditionTarget = conditionTarget;
    }
}