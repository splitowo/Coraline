package com.bzs2.kiritrader.jackson;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("unused")
/* getters and setters are used by jackson */

public class JacksonOrderUuid {

    /* capital first letter is non-standard, therefore annotations are required */
    @JsonProperty("uuid")
    private String uuid;

    public JacksonOrderUuid() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}